import os

print("Jay Ganesh!")

# Open a file
fo = open("foo.txt", "wb")
fo.write("Python is a great language.\nJay Nana!\n")

# Close opened file
fo.close()

fo1 = open("foo.txt", "r")
print(fo1.read())

fo1.close()

os.mkdir("Test Folder")