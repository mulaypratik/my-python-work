import pymssql


class PythonDbConnect:

 def __init__(self, name):
  self.name = name

 def PrintRecordsFromDatabase(self):
  conn = pymssql.connect(host=r"localhost", user='sa', password='sscs@password', database='DB_CCMS')
  # conn = pymssql.connect(host=r"192.168.0.10", user='sa', password='Cyber@987', database='DB_Test1')
  print("Connected to DB.")
  cursor = conn.cursor()
  cursor.execute('SELECT * FROM Tbl_Admin_Master;')
  row = cursor.fetchone()
  while row:
    print (str(row[0]) + " " + str(row[1]) + " " + str(row[2]) + " " + str(row[3]) + " " + str(row[4]))
    row = cursor.fetchone()


dbObj = PythonDbConnect("Connect MS SQL")
dbObj.PrintRecordsFromDatabase()
